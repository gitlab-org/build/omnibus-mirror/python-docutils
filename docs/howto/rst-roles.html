<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta charset="utf-8" />
<meta name="generator" content="Docutils 0.21b.dev: https://docutils.sourceforge.io/" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="author" content="David Goodger" />
<meta name="dcterms.date" content="$Date$" />
<meta name="dcterms.rights" content="This document has been placed in the public domain." />
<title>Creating reStructuredText Interpreted Text Roles</title>
<link rel="schema.dcterms" href="http://purl.org/dc/terms/"/>
<link rel="stylesheet" href="../../docutils/writers/html5_polyglot/minimal.css" type="text/css" />
<link rel="stylesheet" href="../../docutils/writers/html5_polyglot/responsive.css" type="text/css" />
</head>
<body class="with-toc">
<header>
<p><a class="reference external" href="https://docutils.sourceforge.io">Docutils</a> | <a class="reference external" href="../index.html">Overview</a> | <a class="reference external" href="../index.html#project-fundamentals">About</a> | <a class="reference external" href="../index.html#user">Users</a> | <a class="reference external" href="../index.html#ref">Reference</a> | <a class="reference external" href="../index.html#howto">Developers</a></p>
</header>
<main id="creating-restructuredtext-interpreted-text-roles">
<h1 class="title">Creating reStructuredText Interpreted Text Roles</h1>
<dl class="docinfo">
<dt class="authors">Authors<span class="colon">:</span></dt>
<dd class="authors"><p>David Goodger</p>
</dd>
<dt class="contact">Contact<span class="colon">:</span></dt>
<dd class="contact"><a class="reference external" href="mailto:docutils-develop&#64;lists.sourceforge.net">docutils-develop&#64;lists.sourceforge.net</a></dd>
<dt class="date">Date<span class="colon">:</span></dt>
<dd class="date">$Date$</dd>
<dt class="revision">Revision<span class="colon">:</span></dt>
<dd class="revision">$Revision$</dd>
<dt class="copyright">Copyright<span class="colon">:</span></dt>
<dd class="copyright">This document has been placed in the public domain.</dd>
</dl>
<!-- Minimal menu bar for inclusion in documentation sources
in ``docutils/docs/*/`` sub-diretories.

Attention: this is not a standalone document. -->
<p>Interpreted text roles are an extension mechanism for inline markup in
reStructuredText.  This document aims to make the creation of new
roles as easy and understandable as possible.</p>
<p>Standard roles are described in <a class="reference external" href="../ref/rst/roles.html">reStructuredText Interpreted Text
Roles</a>.  See the <a class="reference external" href="../ref/rst/restructuredtext.html#interpreted-text">Interpreted Text</a> section in the <a class="reference external" href="../ref/rst/restructuredtext.html">reStructuredText
Markup Specification</a> for syntax details.</p>
<nav class="contents" id="contents" role="doc-toc">
<p class="topic-title">Contents</p>
<ul class="simple">
<li><p><a class="reference internal" href="#define-the-role-function" id="toc-entry-1">Define the Role Function</a></p></li>
<li><p><a class="reference internal" href="#specify-role-function-options-and-content" id="toc-entry-2">Specify Role Function Options and Content</a></p></li>
<li><p><a class="reference internal" href="#register-the-role" id="toc-entry-3">Register the Role</a></p></li>
<li><p><a class="reference internal" href="#examples" id="toc-entry-4">Examples</a></p>
<ul>
<li><p><a class="reference internal" href="#generic-roles" id="toc-entry-5">Generic Roles</a></p></li>
<li><p><a class="reference internal" href="#rfc-reference-role" id="toc-entry-6">RFC Reference Role</a></p></li>
</ul>
</li>
</ul>
</nav>
<section id="define-the-role-function">
<h2><a class="toc-backref" href="#toc-entry-1" role="doc-backlink">Define the Role Function</a><a class="self-link" title="link to this section" href="#define-the-role-function"></a></h2>
<p>The role function creates and returns inline elements (nodes) and does
any additional processing required.  Its signature is as follows:</p>
<pre class="literal-block">def role_fn(name, rawtext, text, lineno, inliner,
            options=None, content=None):
    code...

# Optional function attributes for customization:
role_fn.options = ...
role_fn.content = ...</pre>
<p>Function attributes are described below (see <a class="reference internal" href="#specify-role-function-options-and-content">Specify Role Function
Options and Content</a>).  The role function parameters are as follows:</p>
<ul class="simple">
<li><p><span class="docutils literal">name</span>: The local name of the interpreted role, the role name
actually used in the document.</p></li>
<li><p><span class="docutils literal">rawtext</span>: A string containing the entire interpreted text input,
including the role and markup.  Return it as a <span class="docutils literal">problematic</span> node
linked to a system message if a problem is encountered.</p></li>
<li><p><span class="docutils literal">text</span>: The interpreted text content.</p></li>
<li><p><span class="docutils literal">lineno</span>: The line number where the text block containing the
interpreted text begins.</p></li>
<li><p><span class="docutils literal">inliner</span>: The <span class="docutils literal">docutils.parsers.rst.states.Inliner</span> object that
called role_fn.  It contains the several attributes useful for error
reporting and document tree access.</p></li>
<li><p><span class="docutils literal">options</span>: A dictionary of directive options for customization
(from the <a class="reference external" href="../ref/rst/directives.html#role">&quot;role&quot; directive</a>), to be interpreted by the role
function.  Used for additional attributes for the generated elements
and other functionality.</p></li>
<li><p><span class="docutils literal">content</span>: A list of strings, the directive content for
customization (from the <a class="reference external" href="../ref/rst/directives.html#role">&quot;role&quot; directive</a>).  To be interpreted by
the role function.</p></li>
</ul>
<p>Role functions return a tuple of two values:</p>
<ul class="simple">
<li><p>A list of nodes which will be inserted into the document tree at the
point where the interpreted role was encountered (can be an empty
list).</p></li>
<li><p>A list of system messages, which will be inserted into the document tree
immediately after the end of the current block (can also be empty).</p></li>
</ul>
</section>
<section id="specify-role-function-options-and-content">
<h2><a class="toc-backref" href="#toc-entry-2" role="doc-backlink">Specify Role Function Options and Content</a><a class="self-link" title="link to this section" href="#specify-role-function-options-and-content"></a></h2>
<p>Function attributes are for customization, and are interpreted by the
<a class="reference external" href="../ref/rst/directives.html#role">&quot;role&quot; directive</a>.  If unspecified, role function attributes are
assumed to have the value <span class="docutils literal">None</span>.  Two function attributes are
recognized:</p>
<ul>
<li><p><span class="docutils literal">options</span>: The option specification.  All role functions
implicitly support the &quot;class&quot; option, unless disabled with an
explicit <span class="docutils literal">{'class': None}</span>.</p>
<p>An option specification must be defined detailing the options
available to the &quot;role&quot; directive.  An option spec is a mapping of
option name to conversion function; conversion functions are applied
to each option value to check validity and convert them to the
expected type.  Python's built-in conversion functions are often
usable for this, such as <span class="docutils literal">int</span>, <span class="docutils literal">float</span>, and <span class="docutils literal">bool</span> (included
in Python from version 2.2.1).  Other useful conversion functions
are included in the <span class="docutils literal">docutils.parsers.rst.directives</span> package.
For further details, see <a class="reference external" href="rst-directives.html#specify-directive-arguments-options-and-content">Creating reStructuredText Directives</a>.</p>
</li>
<li><p><span class="docutils literal">content</span>: A boolean; true if &quot;role&quot; directive content is allowed.
Role functions must handle the case where content is required but
not supplied (an empty content list will be supplied).</p>
<p>As of this writing, no roles accept directive content.</p>
</li>
</ul>
<p>Note that unlike directives, the &quot;arguments&quot; function attribute is not
supported for role customization.  Directive arguments are handled by
the &quot;role&quot; directive itself.</p>
</section>
<section id="register-the-role">
<h2><a class="toc-backref" href="#toc-entry-3" role="doc-backlink">Register the Role</a><a class="self-link" title="link to this section" href="#register-the-role"></a></h2>
<p>If the role is a general-use addition to the Docutils core, it must be
registered with the parser and language mappings added:</p>
<ol class="arabic">
<li><p>Register the new role using the canonical name:</p>
<pre class="literal-block">from docutils.parsers.rst import roles
roles.register_canonical_role(name, role_function)</pre>
<p>This code is normally placed immediately after the definition of
the role function.</p>
</li>
<li><p>Add an entry to the <span class="docutils literal">roles</span> dictionary in
<span class="docutils literal">docutils/parsers/rst/languages/en.py</span> for the role, mapping the
English name to the canonical name (both lowercase).  Usually the
English name and the canonical name are the same.  Abbreviations
and other aliases may also be added here.</p></li>
<li><p>Update all the other language modules as well.  For languages in
which you are proficient, please add translations.  For other
languages, add the English role name plus &quot;(translation required)&quot;.</p></li>
</ol>
<p>If the role is application-specific, use the <span class="docutils literal">register_local_role</span>
function:</p>
<pre class="literal-block">from docutils.parsers.rst import roles
roles.register_local_role(name, role_function)</pre>
</section>
<section id="examples">
<h2><a class="toc-backref" href="#toc-entry-4" role="doc-backlink">Examples</a><a class="self-link" title="link to this section" href="#examples"></a></h2>
<p>For the most direct and accurate information, &quot;Use the Source, Luke!&quot;.
All standard roles are documented in <a class="reference external" href="../ref/rst/roles.html">reStructuredText Interpreted
Text Roles</a>, and the source code implementing them is located in the
<span class="docutils literal">docutils/parsers/rst/roles.py</span> module.  Several representative
roles are described below.</p>
<section id="generic-roles">
<h3><a class="toc-backref" href="#toc-entry-5" role="doc-backlink">Generic Roles</a><a class="self-link" title="link to this section" href="#generic-roles"></a></h3>
<p>Many roles simply wrap a given element around the text.  There's a
special helper function, <span class="docutils literal">register_generic_role</span>, which generates a
role function from the canonical role name and node class:</p>
<pre class="literal-block">register_generic_role('emphasis', nodes.emphasis)</pre>
<p>For the implementation of <span class="docutils literal">register_generic_role</span>, see the
<span class="docutils literal">docutils.parsers.rst.roles</span> module.</p>
</section>
<section id="rfc-reference-role">
<h3><a class="toc-backref" href="#toc-entry-6" role="doc-backlink">RFC Reference Role</a><a class="self-link" title="link to this section" href="#rfc-reference-role"></a></h3>
<p>This role allows easy references to <a class="reference external" href="http://foldoc.doc.ic.ac.uk/foldoc/foldoc.cgi?query=rfc&amp;action=Search&amp;sourceid=Mozilla-search">RFCs</a> (Request For Comments
documents) by automatically providing the base URL,
<a class="reference external" href="http://www.faqs.org/rfcs/">http://www.faqs.org/rfcs/</a>, and appending the RFC document itself
(rfcXXXX.html, where XXXX is the RFC number).  For example:</p>
<pre class="literal-block">See :RFC:`2822` for information about email headers.</pre>
<p>This is equivalent to:</p>
<pre class="literal-block">See `RFC 2822`__ for information about email headers.

__ http://www.faqs.org/rfcs/rfc2822.html</pre>
<p>Here is the implementation of the role:</p>
<pre class="literal-block">def rfc_reference_role(role, rawtext, text, lineno, inliner,
                       options=None, content=None):
    if &quot;#&quot; in text:
        rfcnum, section = utils.unescape(text).split(&quot;#&quot;, 1)
    else:
        rfcnum, section  = utils.unescape(text), None
    try:
        rfcnum = int(rfcnum)
        if rfcnum &lt; 1:
            raise ValueError
    except ValueError:
        msg = inliner.reporter.error(
            'RFC number must be a number greater than or equal to 1; '
            '&quot;%s&quot; is invalid.' % text, line=lineno)
        prb = inliner.problematic(rawtext, rawtext, msg)
        return [prb], [msg]
    # Base URL mainly used by inliner.rfc_reference, so this is correct:
    ref = inliner.document.settings.rfc_base_url + inliner.rfc_url % rfcnum
    if section is not None:
        ref += &quot;#&quot;+section
    options = normalize_role_options(options)
    node = nodes.reference(rawtext, 'RFC ' + str(rfcnum), refuri=ref,
                           **options)
    return [node], []

register_canonical_role('rfc-reference', rfc_reference_role)</pre>
<p>Noteworthy in the code above are:</p>
<ol class="arabic simple">
<li><p>The interpreted text itself should contain the RFC number.  The
<span class="docutils literal">try</span> clause verifies by converting it to an integer.  If the
conversion fails, the <span class="docutils literal">except</span> clause is executed: a system
message is generated, the entire interpreted text construct (in
<span class="docutils literal">rawtext</span>) is wrapped in a <span class="docutils literal">problematic</span> node (linked to the
system message), and the two are returned.</p></li>
<li><p>The RFC reference itself is constructed from a stock URI, set as
the &quot;refuri&quot; attribute of a &quot;reference&quot; element.</p></li>
<li><p>The <span class="docutils literal">options</span> function parameter, a dictionary, may contain a
&quot;class&quot; customization attribute; it is interpreted and replaced
with a &quot;classes&quot; attribute by the <span class="docutils literal">set_classes()</span> function.  The
resulting &quot;classes&quot; attribute is passed through to the &quot;reference&quot;
element node constructor.</p></li>
</ol>
</section>
</section>
</main>
<footer>
<p><a class="reference external" href="rst-roles.txt">View document source</a>.
Generated on: 2023-09-13 17:06 UTC.
Generated by <a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> from <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> source.
</p>
</footer>
</body>
</html>
