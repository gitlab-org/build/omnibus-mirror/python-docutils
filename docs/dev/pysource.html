<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta charset="utf-8" />
<meta name="generator" content="Docutils 0.21b.dev: https://docutils.sourceforge.io/" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="author" content="David Goodger" />
<meta name="dcterms.date" content="$Date$" />
<meta name="dcterms.rights" content="This document has been placed in the public domain." />
<title>Python Source Reader</title>
<link rel="schema.dcterms" href="http://purl.org/dc/terms/"/>
<link rel="stylesheet" href="../../docutils/writers/html5_polyglot/minimal.css" type="text/css" />
<link rel="stylesheet" href="../../docutils/writers/html5_polyglot/responsive.css" type="text/css" />
</head>
<body class="with-toc">
<header>
<p><a class="reference external" href="https://docutils.sourceforge.io">Docutils</a> | <a class="reference external" href="../index.html">Overview</a> | <a class="reference external" href="../index.html#project-fundamentals">About</a> | <a class="reference external" href="../index.html#user">Users</a> | <a class="reference external" href="../index.html#ref">Reference</a> | <a class="reference external" href="../index.html#howto">Developers</a></p>
</header>
<main id="python-source-reader">
<h1 class="title">Python Source Reader</h1>
<dl class="docinfo">
<dt class="author">Author<span class="colon">:</span></dt>
<dd class="author"><p>David Goodger</p></dd>
<dt class="contact">Contact<span class="colon">:</span></dt>
<dd class="contact"><a class="reference external" href="mailto:docutils-develop&#64;lists.sourceforge.net">docutils-develop&#64;lists.sourceforge.net</a></dd>
<dt class="revision">Revision<span class="colon">:</span></dt>
<dd class="revision">$Revision$</dd>
<dt class="date">Date<span class="colon">:</span></dt>
<dd class="date">$Date$</dd>
<dt class="copyright">Copyright<span class="colon">:</span></dt>
<dd class="copyright">This document has been placed in the public domain.</dd>
</dl>
<!-- Minimal menu bar for inclusion in documentation sources
in ``docutils/docs/*/`` sub-diretories.

Attention: this is not a standalone document. -->
<p>This document explores issues around extracting and processing
docstrings from Python modules.</p>
<p>For definitive element hierarchy details, see the &quot;Python Plaintext
Document Interface DTD&quot; XML document type definition, <a class="reference external" href="pysource.dtd">pysource.dtd</a>
(which modifies the generic <a class="reference external" href="../ref/docutils.dtd">docutils.dtd</a>).  Descriptions below list
'DTD elements' (XML 'generic identifiers' or tag names) corresponding
to syntax constructs.</p>
<nav class="contents" id="contents" role="doc-toc">
<p class="topic-title">Contents</p>
<ul class="simple">
<li><p><a class="reference internal" href="#model" id="toc-entry-1">Model</a></p></li>
<li><p><a class="reference internal" href="#docstring-extractor" id="toc-entry-2">Docstring Extractor</a></p></li>
<li><p><a class="reference internal" href="#interpreted-text" id="toc-entry-3">Interpreted Text</a></p></li>
</ul>
</nav>
<section id="model">
<h2><a class="toc-backref" href="#toc-entry-1" role="doc-backlink">Model</a><a class="self-link" title="link to this section" href="#model"></a></h2>
<p>The Python Source Reader (&quot;PySource&quot;) model that's evolving in my mind
goes something like this:</p>
<ol class="arabic">
<li><p>Extract the docstring/namespace <a class="brackets" href="#footnote-1" id="footnote-reference-1" role="doc-noteref"><span class="fn-bracket">[</span>1<span class="fn-bracket">]</span></a> tree from the module(s) and/or
package(s).</p>
<aside class="footnote-list brackets">
<aside class="footnote brackets" id="footnote-1" role="doc-footnote">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#footnote-reference-1">1</a><span class="fn-bracket">]</span></span>
<p>See <a class="reference internal" href="#docstring-extractor">Docstring Extractor</a> below.</p>
</aside>
</aside>
</li>
<li><p>Run the parser on each docstring in turn, producing a forest of
doctrees (per nodes.py).</p></li>
<li><p>Join the docstring trees together into a single tree, running
transforms:</p>
<ul class="simple">
<li><p>merge hyperlinks</p></li>
<li><p>merge namespaces</p></li>
<li><p>create various sections like &quot;Module Attributes&quot;, &quot;Functions&quot;,
&quot;Classes&quot;, &quot;Class Attributes&quot;, etc.; see <a class="reference external" href="pysource.dtd">pysource.dtd</a></p></li>
<li><p>convert the above special sections to ordinary doctree nodes</p></li>
</ul>
</li>
<li><p>Run transforms on the combined doctree.  Examples: resolving
cross-references/hyperlinks (including interpreted text on Python
identifiers); footnote auto-numbering; first field list -&gt;
bibliographic elements.</p>
<p>(Or should step 4's transforms come before step 3?)</p>
</li>
<li><p>Pass the resulting unified tree to the writer/builder.</p></li>
</ol>
<p>I've had trouble reconciling the roles of input parser and output
writer with the idea of modes (&quot;readers&quot; or &quot;directors&quot;).  Does the
mode govern the transformation of the input, the output, or both?
Perhaps the mode should be split into two.</p>
<p>For example, say the source of our input is a Python module.  Our
&quot;input mode&quot; should be the &quot;Python Source Reader&quot;.  It discovers (from
<span class="docutils literal">__docformat__</span>) that the input parser is &quot;reStructuredText&quot;.  If we
want HTML, we'll specify the &quot;HTML&quot; output formatter.  But there's a
piece missing.  What <em>kind</em> or <em>style</em> of HTML output do we want?
PyDoc-style, LibRefMan style, etc.  (many people will want to specify
and control their own style).  Is the output style specific to a
particular output format (XML, HTML, etc.)?  Is the style specific to
the input mode?  Or can/should they be independent?</p>
<p>I envision interaction between the input parser, an &quot;input mode&quot; , and
the output formatter.  The same intermediate data format would be used
between each of these, being transformed as it progresses.</p>
</section>
<section id="docstring-extractor">
<h2><a class="toc-backref" href="#toc-entry-2" role="doc-backlink">Docstring Extractor</a><a class="self-link" title="link to this section" href="#docstring-extractor"></a></h2>
<p>We need code that scans a parsed Python module, and returns an ordered
tree containing the names, docstrings (including attribute and
additional docstrings), and additional info (in parentheses below) of
all of the following objects:</p>
<ul class="simple">
<li><p>packages</p></li>
<li><p>modules</p></li>
<li><p>module attributes (+ values)</p></li>
<li><p>classes (+ inheritance)</p></li>
<li><p>class attributes (+ values)</p></li>
<li><p>instance attributes (+ values)</p></li>
<li><p>methods (+ formal parameters &amp; defaults)</p></li>
<li><p>functions (+ formal parameters &amp; defaults)</p></li>
</ul>
<p>(Extract comments too?  For example, comments at the start of a module
would be a good place for bibliographic field lists.)</p>
<p>In order to evaluate interpreted text cross-references, namespaces for
each of the above will also be required.</p>
<p>See python-dev/docstring-develop thread &quot;AST mining&quot;, started on
2001-08-14.</p>
</section>
<section id="interpreted-text">
<h2><a class="toc-backref" href="#toc-entry-3" role="doc-backlink">Interpreted Text</a><a class="self-link" title="link to this section" href="#interpreted-text"></a></h2>
<p>DTD elements: package, module, class, method, function,
module_attribute, class_attribute, instance_attribute, variable,
parameter, type, exception_class, warning_class.</p>
<p>To classify identifiers explicitly, the role is given along with the
identifier in either prefix or suffix form:</p>
<pre class="literal-block">Use :method:`Keeper.storedata` to store the object's data in
`Keeper.data`:instance_attribute:.</pre>
<p>The role may be one of 'package', 'module', 'class', 'method',
'function', 'module_attribute', 'class_attribute',
'instance_attribute', 'variable', 'parameter', 'type',
'exception_class', 'exception', 'warning_class', or 'warning'.  Other
roles may be defined.</p>
<!-- Local Variables:
mode: indented-text
indent-tabs-mode: nil
fill-column: 70
End: -->
</section>
</main>
<footer>
<p><a class="reference external" href="pysource.txt">View document source</a>.
Generated on: 2023-09-13 17:06 UTC.
Generated by <a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> from <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> source.
</p>
</footer>
</body>
</html>
