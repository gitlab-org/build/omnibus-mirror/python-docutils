<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta charset="utf-8" />
<meta name="generator" content="Docutils 0.21b.dev: https://docutils.sourceforge.io/" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="author" content="David Goodger" />
<meta name="dcterms.date" content="$Date$" />
<meta name="dcterms.rights" content="This document has been placed in the public domain." />
<title>Docutils Project Documentation Overview</title>
<link rel="schema.dcterms" href="http://purl.org/dc/terms/"/>
<link rel="stylesheet" href="../docutils/writers/html5_polyglot/minimal.css" type="text/css" />
<link rel="stylesheet" href="../docutils/writers/html5_polyglot/responsive.css" type="text/css" />
</head>
<body class="with-toc">
<header>
<p><a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> | Overview | <a class="reference internal" href="#project-fundamentals">About</a> | <a class="reference internal" href="#user">Users</a> | <a class="reference internal" href="#ref">Reference</a> | <a class="reference internal" href="#howto">Developers</a></p>
</header>
<main id="docutils-project-documentation-overview">
<h1 class="title">Docutils Project Documentation Overview</h1>
<dl class="docinfo">
<dt class="author">Author<span class="colon">:</span></dt>
<dd class="author"><p>David Goodger</p></dd>
<dt class="contact">Contact<span class="colon">:</span></dt>
<dd class="contact"><a class="reference external" href="mailto:docutils-develop&#64;lists.sourceforge.net">docutils-develop&#64;lists.sourceforge.net</a></dd>
<dt class="date">Date<span class="colon">:</span></dt>
<dd class="date">$Date$</dd>
<dt class="revision">Revision<span class="colon">:</span></dt>
<dd class="revision">$Revision$</dd>
<dt class="copyright">Copyright<span class="colon">:</span></dt>
<dd class="copyright">This document has been placed in the public domain.</dd>
</dl>
<p>The latest working documents may be accessed individually below, or
from the <span class="docutils literal">docs</span> directory of the <a class="reference external" href="https://docutils.sourceforge.io/#download">Docutils distribution</a>.</p>
<nav class="contents" id="contents" role="doc-toc">
<p class="topic-title">Contents</p>
<ul class="simple">
<li><p><a class="reference internal" href="#project-fundamentals" id="toc-entry-1">Project Fundamentals</a></p></li>
<li><p><a class="reference internal" href="#introductory-tutorial-material-for-end-users" id="toc-entry-2">Introductory &amp; Tutorial Material for End-Users</a></p></li>
<li><p><a class="reference internal" href="#reference-material-for-all-groups" id="toc-entry-3">Reference Material for All Groups</a></p></li>
<li><p><a class="reference internal" href="#api-reference-material-for-client-developers" id="toc-entry-4">API Reference Material for Client-Developers</a></p></li>
<li><p><a class="reference internal" href="#instructions-for-developers" id="toc-entry-5">Instructions for Developers</a></p></li>
<li><p><a class="reference internal" href="#development-notes-and-plans-for-core-developers" id="toc-entry-6">Development Notes and Plans for Core-Developers</a></p></li>
</ul>
</nav>
<section id="project-fundamentals">
<h2><a class="toc-backref" href="#toc-entry-1" role="doc-backlink">Project Fundamentals</a><a class="self-link" title="link to this section" href="#project-fundamentals"></a></h2>
<p>These files are for all <a class="reference internal" href="#docutils-stakeholders">Docutils stakeholders</a>.  They are kept at the
top level of the Docutils project directory.</p>
<dl class="narrow run-in field-list simple">
<dt><a class="reference external" href="../README.html">README</a><span class="colon">:</span></dt>
<dd><p>Project overview: quick-start, requirements,
installation, and usage.</p>
</dd>
<dt><a class="reference external" href="../COPYING.html">COPYING</a><span class="colon">:</span></dt>
<dd><p>Conditions for Docutils redistribution, with links to
licenses.</p>
</dd>
<dt><a class="reference external" href="../FAQ.html">FAQ</a><span class="colon">:</span></dt>
<dd><p>Docutils Frequently Asked Questions.  If you have a
question or issue, there's a good chance it's already
answered here.</p>
</dd>
<dt><a class="reference external" href="../BUGS.html">BUGS</a><span class="colon">:</span></dt>
<dd><p>A list of known bugs, and how to report a bug.</p>
</dd>
<dt><a class="reference external" href="../RELEASE-NOTES.html">RELEASE-NOTES</a><span class="colon">:</span></dt>
<dd><p>Summary of the major changes in recent releases and
notice of future incompatible changes.</p>
</dd>
<dt><a class="reference external" href="../HISTORY.html">HISTORY</a><span class="colon">:</span></dt>
<dd><p>Detailed change history log.</p>
</dd>
<dt><a class="reference external" href="../THANKS.html">THANKS</a><span class="colon">:</span></dt>
<dd><p>Acknowledgements.</p>
</dd>
</dl>
<div class="details" id="docutils-stakeholders">
<details>
<summary>Docutils Stakeholders …</summary>
<p>can be categorized in several groups:</p>
<dl class="simple">
<dt>End-users:</dt>
<dd><p>users of reStructuredText and the Docutils tools.
Although some are developers (e.g. Python developers utilizing
reStructuredText for docstrings in their source), many are not.</p>
</dd>
<dt>Client-developers:</dt>
<dd><p>developers using Docutils as a library,
programmers developing <em>with</em> Docutils.</p>
</dd>
<dt>Component-developers:</dt>
<dd><p>those who implement application-specific components,
directives, and/or roles, separately from Docutils.</p>
</dd>
<dt>Core-developers:</dt>
<dd><p>developers of the Docutils codebase and participants
in the Docutils project community.</p>
</dd>
<dt>Re-implementers:</dt>
<dd><p>developers of alternate implementations of Docutils.</p>
</dd>
</dl>
<p>There's a lot of overlap between these groups.  Most (perhaps all)
core-developers, component-developers, client-developers, and
re-implementers are also end-users.  Core-developers are also
client-developers, and may also be component-developers in other
projects.  Component-developers are also client-developers.</p>
</details>
</div>
</section>
<section id="introductory-tutorial-material-for-end-users">
<span id="user"></span><h2><a class="toc-backref" href="#toc-entry-2" role="doc-backlink">Introductory &amp; Tutorial Material for End-Users</a><a class="self-link" title="link to this section" href="#introductory-tutorial-material-for-end-users"></a></h2>
<dl class="simple">
<dt>Docutils-general:</dt>
<dd><ul class="simple">
<li><p><a class="reference external" href="user/tools.html">Docutils Front-End Tools</a></p></li>
<li><p><a class="reference external" href="user/config.html">Docutils Configuration</a></p></li>
<li><p><a class="reference external" href="user/mailing-lists.html">Docutils Mailing Lists</a></p></li>
<li><p><a class="reference external" href="user/links.html">Docutils Link List</a></p></li>
</ul>
</dd>
<dt>Writer-specific:</dt>
<dd><ul class="simple">
<li><p><a class="reference external" href="user/html.html">Docutils HTML Writers</a></p></li>
<li><p><a class="reference external" href="user/slide-shows.html">Easy Slide Shows With reStructuredText &amp; S5</a></p></li>
<li><p><a class="reference external" href="user/latex.html">Docutils LaTeX Writer</a></p></li>
<li><p><a class="reference external" href="user/manpage.html">Man Page Writer for Docutils</a></p></li>
<li><p><a class="reference external" href="user/odt.html">Docutils ODF/OpenOffice/odt Writer</a></p></li>
</ul>
</dd>
<dt><a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a>:</dt>
<dd><ul class="simple">
<li><p><a class="reference external" href="user/rst/quickstart.html">A ReStructuredText Primer</a>
(see also the <a class="reference external" href="user/rst/quickstart.txt">text source</a>)</p></li>
<li><p><a class="reference external" href="user/rst/quickref.html">Quick reStructuredText</a> (user reference)</p></li>
<li><p><a class="reference external" href="user/rst/cheatsheet.txt">reStructuredText Cheat Sheet</a> (text
only; 1 page for syntax, 1 page directive &amp; role reference)</p></li>
<li><p><a class="reference external" href="user/rst/demo.html">Demonstration</a>
of most reStructuredText features
(see also the <a class="reference external" href="user/rst/demo.txt">text source</a>)</p></li>
</ul>
</dd>
<dt>Editor support:</dt>
<dd><ul class="simple">
<li><p><a class="reference external" href="user/emacs.html">Emacs support for reStructuredText</a></p></li>
</ul>
</dd>
</dl>
</section>
<section id="reference-material-for-all-groups">
<span id="ref"></span><h2><a class="toc-backref" href="#toc-entry-3" role="doc-backlink">Reference Material for All Groups</a><a class="self-link" title="link to this section" href="#reference-material-for-all-groups"></a></h2>
<p>Many of these files began as developer specifications, but now that
they're mature and used by end-users and client-developers, they have
become reference material.  Successful specs evolve into refs.</p>
<dl class="simple">
<dt>Docutils-general:</dt>
<dd><ul class="simple">
<li><p><a class="reference external" href="ref/doctree.html">The Docutils Document Tree</a> (incomplete)</p></li>
<li><p><a class="reference external" href="ref/docutils.dtd">Docutils Generic DTD</a></p></li>
<li><p><a class="reference external" href="ref/soextblx.dtd">OASIS XML Exchange Table Model Declaration Module</a> (CALS tables DTD module)</p></li>
<li><p><a class="reference external" href="peps/pep-0258.html">Docutils Design Specification</a> (PEP 258)</p></li>
</ul>
</dd>
<dt><a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a>:</dt>
<dd><ul class="simple">
<li><p><a class="reference external" href="ref/rst/introduction.html">An Introduction to reStructuredText</a>
(includes the <a class="reference external" href="ref/rst/introduction.html#goals">Goals</a> and
<a class="reference external" href="ref/rst/introduction.html#history">History</a> of reStructuredText)</p></li>
<li><p><a class="reference external" href="ref/rst/restructuredtext.html">reStructuredText Markup Specification</a></p></li>
<li><p><a class="reference external" href="ref/rst/directives.html">reStructuredText Directives</a></p></li>
<li><p><a class="reference external" href="ref/rst/roles.html">reStructuredText Interpreted Text Roles</a></p></li>
<li><p><a class="reference external" href="ref/rst/definitions.html">reStructuredText Standard Definition Files</a></p></li>
<li><p><a class="reference external" href="ref/rst/mathematics.html">LaTeX syntax for mathematics</a>
(syntax used in &quot;math&quot; directive and role)</p></li>
</ul>
</dd>
</dl>
<dl id="peps">
<dt>Python Enhancement Proposals</dt>
<dd><ul class="simple">
<li><p><a class="reference external" href="peps/pep-0256.html">PEP 256: Docstring Processing System Framework</a> is a high-level
generic proposal.  [<a class="reference external" href="https://peps.python.org/pep-0256">PEP 256</a> in the <a class="reference external" href="https://peps.python.org">master repository</a>]</p></li>
<li><p><a class="reference external" href="peps/pep-0257.html">PEP 257: Docstring Conventions</a> addresses docstring style and
touches on content.  [<a class="reference external" href="https://peps.python.org/pep-0257">PEP 257</a> in the <a class="reference external" href="https://peps.python.org">master repository</a>]</p></li>
<li><p><a class="reference external" href="peps/pep-0258.html">PEP 258: Docutils Design Specification</a> is an overview of the
architecture of Docutils.  It documents design issues and
implementation details.  [<a class="reference external" href="https://peps.python.org/pep-0258">PEP 258</a> in the <a class="reference external" href="https://peps.python.org">master repository</a>]</p></li>
<li><p><a class="reference external" href="peps/pep-0287.html">PEP 287: reStructuredText Docstring Format</a> proposes a standard
markup syntax.  [<a class="reference external" href="https://peps.python.org/pep-0287">PEP 287</a> in the <a class="reference external" href="https://peps.python.org">master repository</a>]</p></li>
</ul>
<p>Please note that PEPs in the <a class="reference external" href="https://peps.python.org">master repository</a> developed
independent from the local versions after submission.</p>
</dd>
<dt>Prehistoric:</dt>
<dd><p><a class="reference external" href="https://docutils.sourceforge.io/mirror/setext.html">Setext Documents Mirror</a></p>
</dd>
</dl>
</section>
<section id="api-reference-material-for-client-developers">
<span id="api"></span><h2><a class="toc-backref" href="#toc-entry-4" role="doc-backlink">API Reference Material for Client-Developers</a><a class="self-link" title="link to this section" href="#api-reference-material-for-client-developers"></a></h2>
<dl class="simple">
<dt><a class="reference external" href="api/publisher.html">The Docutils Publisher</a></dt>
<dd><p>entry points for using Docutils as a library</p>
</dd>
<dt><a class="reference external" href="api/runtime-settings.html">Docutils Runtime Settings</a></dt>
<dd><p>configuration framework details</p>
</dd>
<dt><a class="reference external" href="api/transforms.html">Docutils Transforms</a></dt>
<dd><p>change the document tree in-place (resolve references, …)</p>
</dd>
</dl>
<p>The <a class="reference external" href="peps/pep-0258.html">Docutils Design Specification</a> (PEP 258) is a must-read for any
Docutils developer.</p>
</section>
<section id="instructions-for-developers">
<span id="howto"></span><h2><a class="toc-backref" href="#toc-entry-5" role="doc-backlink">Instructions for Developers</a><a class="self-link" title="link to this section" href="#instructions-for-developers"></a></h2>
<dl class="field-list simple">
<dt>Security<span class="colon">:</span></dt>
<dd><p><a class="reference external" href="howto/security.html">Deploying Docutils Securely</a></p>
</dd>
</dl>
<ul class="simple">
<li><p><a class="reference external" href="howto/cmdline-tool.html">Inside A Docutils Command-Line Front-End Tool</a></p></li>
<li><p><a class="reference external" href="dev/runtime-settings-processing.html">Runtime Settings Processing</a></p></li>
<li><p><a class="reference external" href="howto/html-stylesheets.html">Writing HTML (CSS) Stylesheets for Docutils</a></p></li>
<li><p><a class="reference external" href="howto/i18n.html">Docutils Internationalization</a></p></li>
<li><p><a class="reference external" href="howto/rst-directives.html">Creating reStructuredText Directives</a></p></li>
<li><p><a class="reference external" href="howto/rst-roles.html">Creating reStructuredText Interpreted Text Roles</a></p></li>
</ul>
</section>
<section id="development-notes-and-plans-for-core-developers">
<span id="dev"></span><h2><a class="toc-backref" href="#toc-entry-6" role="doc-backlink">Development Notes and Plans for Core-Developers</a><a class="self-link" title="link to this section" href="#development-notes-and-plans-for-core-developers"></a></h2>
<dl class="simple">
<dt>Docutils-general:</dt>
<dd><ul class="simple">
<li><p><a class="reference external" href="dev/hacking.html">Docutils Hacker's Guide</a></p></li>
<li><p><a class="reference external" href="dev/distributing.html">Docutils Distributor's Guide</a></p></li>
<li><p><a class="reference external" href="dev/todo.html">Docutils To Do List</a></p></li>
<li><p><a class="reference external" href="dev/policies.html">Docutils Project Policies</a></p></li>
<li><p><a class="reference external" href="dev/website.html">Docutils Web Site</a></p></li>
<li><p><a class="reference external" href="dev/release.html">Docutils Release Procedure</a></p></li>
<li><p><a class="reference external" href="dev/repository.html">The Docutils Subversion Repository</a></p></li>
<li><p><a class="reference external" href="dev/testing.html">Docutils Testing</a></p></li>
<li><p><a class="reference external" href="dev/semantics.html">Docstring Semantics</a> (incomplete)</p></li>
<li><p><a class="reference external" href="dev/pysource.html">Python Source Reader</a> (incomplete)</p></li>
<li><p><a class="reference external" href="dev/pysource.dtd">Docutils Python DTD</a></p></li>
<li><p><a class="reference external" href="dev/enthought-plan.html">Plan for Enthought API Documentation Tool</a></p></li>
<li><p><a class="reference external" href="dev/enthought-rfp.html">Enthought API Documentation Tool RFP</a></p></li>
</ul>
</dd>
<dt><a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a>:</dt>
<dd><ul class="simple">
<li><p><a class="reference external" href="dev/rst/alternatives.html">A Record of reStructuredText Syntax Alternatives</a></p></li>
<li><p><a class="reference external" href="dev/rst/problems.html">Problems With StructuredText</a></p></li>
</ul>
</dd>
</dl>
<!-- Local Variables:
mode: indented-text
indent-tabs-mode: nil
sentence-end-double-space: t
fill-column: 70
End: -->
</section>
</main>
<footer>
<p><a class="reference external" href="index.txt">View document source</a>.
Generated on: 2023-09-13 17:06 UTC.
Generated by <a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> from <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> source.
</p>
</footer>
</body>
</html>
