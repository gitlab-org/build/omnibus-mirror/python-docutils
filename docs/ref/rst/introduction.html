<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta charset="utf-8" />
<meta name="generator" content="Docutils 0.21b.dev: https://docutils.sourceforge.io/" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="author" content="David Goodger" />
<meta name="dcterms.date" content="$Date$" />
<meta name="dcterms.rights" content="This document has been placed in the public domain." />
<title>An Introduction to reStructuredText</title>
<link rel="schema.dcterms" href="http://purl.org/dc/terms/"/>
<link rel="stylesheet" href="../../../docutils/writers/html5_polyglot/minimal.css" type="text/css" />
<link rel="stylesheet" href="../../../docutils/writers/html5_polyglot/responsive.css" type="text/css" />
</head>
<body>
<header>
<p><a class="reference external" href="https://docutils.sourceforge.io">Docutils</a> | <a class="reference external" href="../../index.html">Overview</a> | <a class="reference external" href="../../index.html#project-fundamentals">About</a> | <a class="reference external" href="../../index.html#user">Users</a> | <a class="reference external" href="../../index.html#ref">Reference</a> | <a class="reference external" href="../../index.html#howto">Developers</a></p>
</header>
<main id="an-introduction-to-restructuredtext">
<h1 class="title">An Introduction to reStructuredText</h1>
<dl class="docinfo">
<dt class="author">Author<span class="colon">:</span></dt>
<dd class="author"><p>David Goodger</p></dd>
<dt class="contact">Contact<span class="colon">:</span></dt>
<dd class="contact"><a class="reference external" href="mailto:docutils-develop&#64;lists.sourceforge.net">docutils-develop&#64;lists.sourceforge.net</a></dd>
<dt class="revision">Revision<span class="colon">:</span></dt>
<dd class="revision">$Revision$</dd>
<dt class="date">Date<span class="colon">:</span></dt>
<dd class="date">$Date$</dd>
<dt class="copyright">Copyright<span class="colon">:</span></dt>
<dd class="copyright">This document has been placed in the public domain.</dd>
</dl>
<!-- Minimal menu bar for inclusion in documentation sources
in ``docutils/docs/*/`` sub-sub-diretories.

Attention: this is not a standalone document. -->
<p><a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> is an easy-to-read, what-you-see-is-what-you-get
plaintext markup syntax and parser system.  It is useful for inline
program documentation (such as Python docstrings), for quickly
creating simple web pages, and for standalone documents.
<a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> is a proposed revision and reinterpretation of the
<a class="reference external" href="https://zopestructuredtext.readthedocs.org/">StructuredText</a> and <a class="reference external" href="https://docutils.sourceforge.io/mirror/setext.html">Setext</a> lightweight markup systems.</p>
<p>reStructuredText is designed for extensibility for specific
application domains.  Its parser is a component of <a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a>.</p>
<p>This document defines the <a class="reference internal" href="#goals">goals</a> of reStructuredText and provides a
<a class="reference internal" href="#history">history</a> of the project.  It is written using the reStructuredText
markup, and therefore serves as an example of its use.  For a gentle
introduction to using reStructuredText, please read <a class="reference external" href="../../user/rst/quickstart.html">A
ReStructuredText Primer</a>.  The <a class="reference external" href="../../user/rst/quickref.html">Quick reStructuredText</a> user
reference is also useful.  The <a class="reference external" href="restructuredtext.html">reStructuredText Markup
Specification</a> is the definitive reference.  There is also an
analysis of the <a class="reference external" href="../../dev/rst/problems.html">Problems With StructuredText</a>.</p>
<p>ReStructuredText's web page is
<a class="reference external" href="https://docutils.sourceforge.io/rst.html">https://docutils.sourceforge.io/rst.html</a>.</p>
<section id="goals">
<h2>Goals<a class="self-link" title="link to this section" href="#goals"></a></h2>
<p>The primary goal of <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> is to define a markup syntax for
use in Python docstrings and other documentation domains, that is
readable and simple, yet powerful enough for non-trivial use.  The
intended purpose of the reStructuredText markup is twofold:</p>
<ul class="simple">
<li><p>the establishment of a set of standard conventions allowing the
expression of structure within plaintext, and</p></li>
<li><p>the conversion of such documents into useful structured data
formats.</p></li>
</ul>
<p>The secondary goal of reStructuredText is to be accepted by the Python
community (by way of being blessed by PythonLabs and the BDFL <a class="brackets" href="#footnote-1" id="footnote-reference-1" role="doc-noteref"><span class="fn-bracket">[</span>1<span class="fn-bracket">]</span></a>) as
a standard for Python inline documentation (possibly one of several
standards, to account for taste).</p>
<aside class="footnote-list brackets">
<aside class="footnote brackets" id="footnote-1" role="doc-footnote">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#footnote-reference-1">1</a><span class="fn-bracket">]</span></span>
<p>Python's creator and &quot;Benevolent Dictator For Life&quot;,
Guido van Rossum.</p>
</aside>
</aside>
<p>To clarify the primary goal, here are specific design goals, in order,
beginning with the most important:</p>
<ol class="arabic simple">
<li><p>Readable.  The marked-up text must be easy to read without any
prior knowledge of the markup language.  It should be as easily
read in raw form as in processed form.</p></li>
<li><p>Unobtrusive.  The markup that is used should be as simple and
unobtrusive as possible.  The simplicity of markup constructs
should be roughly proportional to their frequency of use.  The most
common constructs, with natural and obvious markup, should be the
simplest and most unobtrusive.  Less common constructs, for which
there is no natural or obvious markup, should be distinctive.</p></li>
<li><p>Unambiguous.  The rules for markup must not be open for
interpretation.  For any given input, there should be one and only
one possible output (including error output).</p></li>
<li><p>Unsurprising.  Markup constructs should not cause unexpected output
upon processing.  As a fallback, there must be a way to prevent
unwanted markup processing when a markup construct is used in a
non-markup context (for example, when documenting the markup syntax
itself).</p></li>
<li><p>Intuitive.  Markup should be as obvious and easily remembered as
possible, for the author as well as for the reader.  Constructs
should take their cues from such naturally occurring sources as
plaintext email messages, newsgroup postings, and text
documentation such as README.txt files.</p></li>
<li><p>Easy.  It should be easy to mark up text using any ordinary text
editor.</p></li>
<li><p>Scalable.  The markup should be applicable regardless of the length
of the text.</p></li>
<li><p>Powerful.  The markup should provide enough constructs to produce a
reasonably rich structured document.</p></li>
<li><p>Language-neutral.  The markup should apply to multiple natural (as
well as artificial) languages, not only English.</p></li>
<li><p>Extensible.  The markup should provide a simple syntax and
interface for adding more complex general markup, and custom
markup.</p></li>
<li><p>Output-format-neutral.  The markup will be appropriate for
processing to multiple output formats, and will not be biased
toward any particular format.</p></li>
</ol>
<p>The design goals above were used as criteria for accepting or
rejecting syntax, or selecting between alternatives.</p>
<p>It is emphatically <em>not</em> the goal of reStructuredText to define
docstring semantics, such as docstring contents or docstring length.
These issues are orthogonal to the markup syntax and beyond the scope
of this specification.</p>
<p>Also, it is not the goal of reStructuredText to maintain compatibility
with <a class="reference external" href="https://zopestructuredtext.readthedocs.org/">StructuredText</a> or <a class="reference external" href="https://docutils.sourceforge.io/mirror/setext.html">Setext</a>.  reStructuredText shamelessly steals
their great ideas and ignores the not-so-great.</p>
<p>Author's note:</p>
<blockquote>
<p>Due to the nature of the problem we're trying to solve (or,
perhaps, due to the nature of the proposed solution), the above
goals unavoidably conflict.  I have tried to extract and distill
the wisdom accumulated over the years in the Python <a class="reference external" href="https://www.python.org/sigs/doc-sig/">Doc-SIG</a>
mailing list and elsewhere, to come up with a coherent and
consistent set of syntax rules, and the above goals by which to
measure them.</p>
<p>There will inevitably be people who disagree with my particular
choices.  Some desire finer control over their markup, others
prefer less.  Some are concerned with very short docstrings,
others with full-length documents.  This specification is an
effort to provide a reasonably rich set of markup constructs in a
reasonably simple form, that should satisfy a reasonably large
group of reasonable people.</p>
<p>David Goodger (<a class="reference external" href="mailto:goodger&#64;python.org">goodger&#64;python.org</a>), 2001-04-20</p>
</blockquote>
</section>
<section id="history">
<h2>History<a class="self-link" title="link to this section" href="#history"></a></h2>
<p><a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a>, the specification, is based on <a class="reference external" href="https://zopestructuredtext.readthedocs.org/">StructuredText</a> and
<a class="reference external" href="https://docutils.sourceforge.io/mirror/setext.html">Setext</a>.  StructuredText was developed by Jim Fulton of <a class="reference external" href="http://www.zope.com">Zope
Corporation</a> (formerly Digital Creations) and first released in 1996.
It is now released as a part of the open-source &quot;Z Object Publishing
Environment&quot; (<a class="reference external" href="https://www.zope.dev">ZOPE</a>).  Ian Feldman's and Tony Sanders' earlier <a class="reference external" href="https://docutils.sourceforge.io/mirror/setext.html">Setext</a>
specification was either an influence on StructuredText or, by their
similarities, at least evidence of the correctness of this approach.</p>
<p>I discovered <a class="reference external" href="https://zopestructuredtext.readthedocs.org/">StructuredText</a> in late 1999 while searching for a way to
document the Python modules in one of my projects.  Version 1.1 of
StructuredText was included in Daniel Larsson's <a class="reference external" href="http://starship.python.net/crew/danilo/pythondoc/">pythondoc</a>.  Although
I was not able to get pythondoc to work for me, I found StructuredText
to be almost ideal for my needs.  I joined the Python <a class="reference external" href="https://www.python.org/sigs/doc-sig/">Doc-SIG</a>
(Documentation Special Interest Group) mailing list and found an
ongoing discussion of the shortcomings of the StructuredText
&quot;standard&quot;.  This discussion has been going on since the inception of
the mailing list in 1996, and possibly predates it.</p>
<p>I decided to modify the original module with my own extensions and
some suggested by the Doc-SIG members.  I soon realized that the
module was not written with extension in mind, so I embarked upon a
general reworking, including adapting it to the &quot;re&quot; regular
expression module (the original inspiration for the name of this
project).  Soon after I completed the modifications, I discovered that
StructuredText.py was up to version 1.23 in the ZOPE distribution.
Implementing the new syntax extensions from version 1.23 proved to be
an exercise in frustration, as the complexity of the module had become
overwhelming.</p>
<p>In 2000, development on StructuredTextNG (&quot;Next Generation&quot;) began at
<a class="reference external" href="http://www.zope.com">Zope Corporation</a> (then Digital Creations).  It seems to have many
improvements, but still suffers from many of the problems of classic
StructuredText.</p>
<p>I decided that a complete rewrite was in order, and even started a
<a class="reference external" href="http://structuredtext.sourceforge.net/">reStructuredText SourceForge project</a> (now inactive).  My
motivations (the &quot;itches&quot; I aim to &quot;scratch&quot;) are as follows:</p>
<ul class="simple">
<li><p>I need a standard format for inline documentation of the programs I
write.  This inline documentation has to be convertible to other
useful formats, such as HTML.  I believe many others have the same
need.</p></li>
<li><p>I believe in the Setext/StructuredText idea and want to help
formalize the standard.  However, I feel the current specifications
and implementations have flaws that desperately need fixing.</p></li>
<li><p>reStructuredText could form part of the foundation for a
documentation extraction and processing system, greatly benefitting
Python.  But it is only a part, not the whole.  reStructuredText is
a markup language specification and a reference parser
implementation, but it does not aspire to be the entire system.  I
don't want reStructuredText or a hypothetical Python documentation
processor to die stillborn because of over-ambition.</p></li>
<li><p>Most of all, I want to help ease the documentation chore, the bane
of many a programmer.</p></li>
</ul>
<p>Unfortunately I was sidetracked and stopped working on this project.
In November 2000 I made the time to enumerate the problems of
StructuredText and possible solutions, and complete the first draft of
a specification.  This first draft was posted to the Doc-SIG in three
parts:</p>
<ul class="simple">
<li><p><a class="reference external" href="https://mail.python.org/pipermail/doc-sig/2000-November/001239.html">A Plan for Structured Text</a></p></li>
<li><p><a class="reference external" href="https://mail.python.org/pipermail/doc-sig/2000-November/001240.html">Problems With StructuredText</a></p></li>
<li><p><a class="reference external" href="https://mail.python.org/pipermail/doc-sig/2000-November/001241.html">reStructuredText: Revised Structured Text Specification</a></p></li>
</ul>
<p>In March 2001 a flurry of activity on the Doc-SIG spurred me to
further revise and refine my specification, the result of which you
are now reading.  An offshoot of the reStructuredText project has been
the realization that a single markup scheme, no matter how well
thought out, may not be enough.  In order to tame the endless debates
on Doc-SIG, a flexible <a class="reference external" href="../../peps/pep-0256.html">Docstring Processing System framework</a> needed
to be constructed.  This framework has become the more important of
the two projects; <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> has found its place as one
possible choice for a single component of the larger framework.</p>
<p>The project web site and the first project release were rolled out in
June 2001, including posting the second draft of the spec <a class="brackets" href="#spec-2" id="footnote-reference-2" role="doc-noteref"><span class="fn-bracket">[</span>2<span class="fn-bracket">]</span></a>
and the first draft of PEPs 256, 257, and 258 <a class="brackets" href="#peps-1" id="footnote-reference-3" role="doc-noteref"><span class="fn-bracket">[</span>3<span class="fn-bracket">]</span></a> to the
Doc-SIG.  These documents and the project implementation proceeded to
evolve at a rapid pace.  Implementation history details can be found
in the <a class="reference external" href="../../../HISTORY.html">project history file</a>.</p>
<p>In November 2001, the reStructuredText parser was nearing completion.
Development of the parser continued with the addition of small
convenience features, improvements to the syntax, the filling in of
gaps, and bug fixes.  After a long holiday break, in early 2002 most
development moved over to the other Docutils components, the
&quot;Readers&quot;, &quot;Writers&quot;, and &quot;Transforms&quot;.  A &quot;standalone&quot; reader
(processes standalone text file documents) was completed in February,
and a basic HTML writer (producing HTML 4.01, using CSS-1) was
completed in early March.</p>
<p><a class="reference external" href="../../peps/pep-0287.html">PEP 287</a>, &quot;reStructuredText Standard Docstring Format&quot;, was created
to formally propose reStructuredText as a standard format for Python
docstrings, PEPs, and other files.  It was first posted to
<a class="reference external" href="news:comp.lang.python">comp.lang.python</a> and the <a class="reference external" href="https://mail.python.org/pipermail/python-dev/">Python-dev</a> mailing list on 2002-04-02.</p>
<p>Version 0.4 of the <a class="reference external" href="http://structuredtext.sourceforge.net/">reStructuredText</a> and <a class="reference external" href="http://docstring.sourceforge.net/">Docstring Processing
System</a> projects were released in April 2002.  The two projects were
immediately merged, renamed to &quot;<a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a>&quot;, and a 0.1 release soon
followed.</p>
<aside class="footnote-list brackets">
<aside class="footnote brackets" id="spec-2" role="doc-footnote">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#footnote-reference-2">2</a><span class="fn-bracket">]</span></span>
<p>The second draft of the spec:</p>
<ul class="simple">
<li><p><a class="reference external" href="https://mail.python.org/pipermail/doc-sig/2001-June/001858.html">An Introduction to reStructuredText</a></p></li>
<li><p><a class="reference external" href="https://mail.python.org/pipermail/doc-sig/2001-June/001859.html">Problems With StructuredText</a></p></li>
<li><p><a class="reference external" href="https://mail.python.org/pipermail/doc-sig/2001-June/001860.html">reStructuredText Markup Specification</a></p></li>
<li><p><a class="reference external" href="https://mail.python.org/pipermail/doc-sig/2001-June/001861.html">Python Extensions to the reStructuredText Markup
Specification</a></p></li>
</ul>
</aside>
<aside class="footnote brackets" id="peps-1" role="doc-footnote">
<span class="label"><span class="fn-bracket">[</span><a role="doc-backlink" href="#footnote-reference-3">3</a><span class="fn-bracket">]</span></span>
<p>First drafts of the PEPs:</p>
<ul class="simple">
<li><p><a class="reference external" href="https://mail.python.org/pipermail/doc-sig/2001-June/001855.html">PEP 256: Docstring Processing System Framework</a></p></li>
<li><p><a class="reference external" href="https://mail.python.org/pipermail/doc-sig/2001-June/001856.html">PEP 258: DPS Generic Implementation Details</a></p></li>
<li><p><a class="reference external" href="https://mail.python.org/pipermail/doc-sig/2001-June/001857.html">PEP 257: Docstring Conventions</a></p></li>
</ul>
<p>Current working versions of the PEPs can be found in
<a class="reference external" href="https://docutils.sourceforge.io/docs/peps/">https://docutils.sourceforge.io/docs/peps/</a>, and official versions
can be found in the <a class="reference external" href="https://peps.python.org/">master PEP repository</a>.</p>
</aside>
</aside>
<!-- Local Variables:
mode: indented-text
indent-tabs-mode: nil
sentence-end-double-space: t
fill-column: 70
End: -->
</section>
</main>
<footer>
<p><a class="reference external" href="introduction.txt">View document source</a>.
Generated on: 2023-09-13 17:06 UTC.
Generated by <a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> from <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> source.
</p>
</footer>
</body>
</html>
