<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta charset="utf-8" />
<meta name="generator" content="Docutils 0.21b.dev: https://docutils.sourceforge.io/" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="author" content="David Goodger; open to all Docutils developers" />
<meta name="dcterms.date" content="$Date$" />
<meta name="dcterms.rights" content="This document has been placed in the public domain." />
<title>Docutils Bugs</title>
<link rel="schema.dcterms" href="http://purl.org/dc/terms/"/>
<link rel="stylesheet" href="docutils/writers/html5_polyglot/minimal.css" type="text/css" />
<link rel="stylesheet" href="docutils/writers/html5_polyglot/responsive.css" type="text/css" />
</head>
<body class="with-toc">
<header>
<p><a class="reference external" href="https://docutils.sourceforge.io">Docutils</a> | <a class="reference external" href="docs/index.html">Overview</a> | <a class="reference external" href="docs/index.html#project-fundamentals">About</a> | <a class="reference external" href="docs/index.html#user">Users</a> | <a class="reference external" href="docs/index.html#ref">Reference</a> | <a class="reference external" href="docs/index.html#howto">Developers</a></p>
</header>
<main id="docutils-bugs">
<h1 class="title"><a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> Bugs</h1>
<dl class="docinfo">
<dt class="author">Author<span class="colon">:</span></dt>
<dd class="author"><p>David Goodger; open to all Docutils developers</p></dd>
<dt class="contact">Contact<span class="colon">:</span></dt>
<dd class="contact"><a class="reference external" href="mailto:goodger&#64;python.org">goodger&#64;python.org</a></dd>
<dt class="date">Date<span class="colon">:</span></dt>
<dd class="date">$Date$</dd>
<dt class="revision">Revision<span class="colon">:</span></dt>
<dd class="revision">$Revision$</dd>
<dt class="copyright">Copyright<span class="colon">:</span></dt>
<dd class="copyright">This document has been placed in the public domain.</dd>
</dl>
<!-- Minimal menu bar for inclusion in documentation sources
in the ``docutils/`` parent diretory.

Attention: this is not a standalone document. -->
<p>Bugs in Docutils?!?  Yes, we do have a few.  Some are old-timers that
tend to stay in the shadows and don't bother anybody.  Once in a while
new bugs are born.  From time to time some bugs (new and old) crawl
out into the light and must be dealt with.  Icky.</p>
<p>This document describes how to report a bug, and lists known bugs.</p>
<nav class="contents" id="contents" role="doc-toc">
<p class="topic-title">Contents</p>
<ul class="simple">
<li><p><a class="reference internal" href="#how-to-report-a-bug" id="toc-entry-1">How To Report A Bug</a></p></li>
<li><p><a class="reference internal" href="#known-bugs" id="toc-entry-2">Known Bugs</a></p></li>
</ul>
</nav>
<section id="how-to-report-a-bug">
<h2><a class="toc-backref" href="#toc-entry-1" role="doc-backlink">How To Report A Bug</a><a class="self-link" title="link to this section" href="#how-to-report-a-bug"></a></h2>
<p>If you think you've discovered a bug, please read through these
guidelines before reporting it.</p>
<p>First, make sure it's a new bug:</p>
<ul>
<li><p>Please check the list of <a class="reference internal" href="#known-bugs">known bugs</a> below and the <a class="reference external" href="https://sourceforge.net/p/docutils/bugs/">SourceForge
Bug Tracker</a> to see if it has already been reported.</p></li>
<li><p>Are you using the very latest version of Docutils?  The bug may have
already been fixed.  Please get the latest version of Docutils from
the <a class="reference external" href="docs/dev/repository.html">repository</a> or from the current <a class="reference external" href="https://docutils.sourceforge.io/#download">snapshot</a> and check again.  Even
if your bug has not been fixed, others probably have, and you're
better off with the most up-to-date code.</p>
<p>If you don't have time to check the latest snapshot, please report
the bug anyway.  We'd rather tell you that it's already fixed than
miss reports of unfixed bugs.</p>
</li>
<li><p>If Docutils does not behave the way you expect, look in the
<a class="reference external" href="docs/">documentation</a> (don't forget the <a class="reference external" href="FAQ.html">FAQ</a>!) and <a class="reference external" href="https://docutils.sourceforge.io/#mailing-lists">mailing list archives</a>
for evidence that it should behave the way you expect.</p></li>
</ul>
<p>If you're not sure, please ask on the <a class="reference external" href="docs/user/mailing-lists.html#docutils-users">Docutils-users</a> mailing list
first.</p>
<hr class="docutils" />
<p>If it's a new bug, the most important thing you can do is to write a
simple description and a recipe that reproduces the bug.  Try to
create a <a class="reference internal" href="#minimal-example">minimal example</a> that demonstrates the bug.  The easier you
make it to understand and track down the bug, the more likely a fix
will be.</p>
<aside class="sidebar" id="minimal-example">
<p class="sidebar-title">minimal example</p>
<p>A <cite>minimal working example</cite> is a complete example which is as as small and
simple as possible. It should be complete and working, so that</p>
<ul class="simple">
<li><p>you cannot accidentally omit information important to diagnosing
the problem and</p></li>
<li><p>the person responding can just copy-and-paste the code to try it out.</p></li>
</ul>
<p>To construct an example which is as small as possible, the rule
quite simple: <em>remove/leave out anything which is not necessary</em>.</p>
<p>See also: <a class="reference external" href="http://www.minimalbeispiel.de/mini-en.html">What is a minimal working example?</a>, <a class="reference external" href="http://www.tex.ac.uk/cgi-bin/texfaq2html?label=minxampl">LaTeX FAQ</a></p>
</aside>
<p>Now you're ready to write the bug report.  Please include:</p>
<ul class="simple">
<li><p>A clear description of the bug.  Describe how you expected Docutils
to behave, and contrast that with how it actually behaved.  While
the bug may seem obvious to you, it may not be so obvious to someone
else, so it's best to avoid a guessing game.</p></li>
<li><p>A complete description of the environment in which you reproduced
the bug:</p>
<ul>
<li><p>Your operating system &amp; version.</p></li>
<li><p>The version of Python (<span class="docutils literal">python <span class="pre">-V</span></span>).</p></li>
<li><p>The version of Docutils (use the &quot;-V&quot; option to most Docutils
front-end tools).</p></li>
<li><p>Any private modifications you made to Docutils.</p></li>
<li><p>Anything else that could possibly be relevant.  Err on the side
of too much information, rather than too little.</p></li>
</ul>
</li>
<li><p>A literal transcript of the <em>exact</em> command you ran, and the <em>exact</em>
output.  Use the &quot;--traceback&quot; option to get a complete picture.</p></li>
<li><p>The exact input and output files.  Create a <a class="reference internal" href="#minimal-example">minimal example</a>
of the failing behaviour — it is better to attach complete files
to your bug report than to include just a summary or excerpt.</p></li>
<li><p>If you also want to include speculation as to the cause, and even a
patch to fix the bug, that would be great!</p></li>
</ul>
<p>The best place to send your bug report is to the <a class="reference external" href="https://sourceforge.net/p/docutils/bugs/">SourceForge Bug
Tracker</a>.  That way, it won't be misplaced or forgotten.  In fact, an
open bug report on SourceForge is a constant irritant that begs to be
squashed.</p>
<p>Thank you!</p>
<p>(This section was inspired by the <a class="reference external" href="http://subversion.tigris.org/">Subversion project's</a> <a class="reference external" href="http://svn.collab.net/viewcvs/svn/trunk/BUGS?view=markup">BUGS</a>
file.)</p>
</section>
<section id="known-bugs">
<h2><a class="toc-backref" href="#toc-entry-2" role="doc-backlink">Known Bugs</a><a class="self-link" title="link to this section" href="#known-bugs"></a></h2>
<p>Also see the <a class="reference external" href="https://sourceforge.net/p/docutils/bugs/">SourceForge Bug Tracker</a>.</p>
<ul>
<li><p id="error-reporting">Calling rst2s5 with a non-existent theme (<span class="docutils literal"><span class="pre">--theme</span> does_not_exist</span>)
causes exceptions.  Such errors should be handled more gracefully.</p>
</li>
<li><p>The &quot;stylesheet&quot; setting (a URL, to be used verbatim) should be
allowed to be combined with &quot;embed_stylesheet&quot;.  The stylesheet data
should be read in using urllib.  There was an assumption that a
stylesheet to be embedded should exist as a file on the local
system, and only the &quot;stylesheet_path&quot; setting should be used.</p></li>
<li><p><span class="docutils literal">utils.relative_path()</span> sometimes returns absolute <span class="target" id="paths-on-windows">paths on
Windows</span> (like <span class="docutils literal"><span class="pre">C:/test/foo.css</span></span>) where it could have chosen a
relative path.</p>
<p>Furthermore, absolute pathnames are inserted verbatim, like
<span class="docutils literal"><span class="pre">href=&quot;C:/test/foo.css&quot;</span></span> instead of
<span class="docutils literal"><span class="pre">href=&quot;file:///C:/test/foo.css&quot;</span></span>.</p>
<!-- gmane web interface is down.
TODO: find this article in the Sourceforge mail archives
For details, see `this posting by Alan G. Isaac
<http://article.gmane.org/gmane.text.docutils.user/1569>`_. -->
</li>
<li><p>Footnote label &quot;5&quot; should be &quot;4&quot; when processing the following
input:</p>
<pre class="literal-block">ref [#abc]_ [#]_ [1]_ [#4]_

.. [#abc] footnote
.. [#] two
.. [1] one
.. [#4] four</pre>
<p>Output:</p>
<pre class="literal-block">&lt;document source=&quot;&lt;stdin&gt;&quot;&gt;
    &lt;paragraph&gt;
        ref
        &lt;footnote_reference auto=&quot;1&quot; ids=&quot;id1&quot; refid=&quot;abc&quot;&gt;
            2

        &lt;footnote_reference auto=&quot;1&quot; ids=&quot;id2&quot; refid=&quot;id5&quot;&gt;
            3

        &lt;footnote_reference ids=&quot;id3&quot; refid=&quot;id6&quot;&gt;
            1

        &lt;footnote_reference auto=&quot;1&quot; ids=&quot;id4&quot; refid=&quot;id7&quot;&gt;
            5
    &lt;footnote auto=&quot;1&quot; backrefs=&quot;id1&quot; ids=&quot;abc&quot; names=&quot;abc&quot;&gt;
        &lt;label&gt;
            2
        &lt;paragraph&gt;
            footnote
    &lt;footnote auto=&quot;1&quot; backrefs=&quot;id2&quot; ids=&quot;id5&quot; names=&quot;3&quot;&gt;
        &lt;label&gt;
            3
        &lt;paragraph&gt;
            two
    &lt;footnote backrefs=&quot;id3&quot; ids=&quot;id6&quot; names=&quot;1&quot;&gt;
        &lt;label&gt;
            1
        &lt;paragraph&gt;
            one
    &lt;footnote auto=&quot;1&quot; backrefs=&quot;id4&quot; ids=&quot;id7&quot; names=&quot;4&quot;&gt;
        &lt;label&gt;
            5
        &lt;paragraph&gt;
            four</pre>
</li>
<li><p>IDs are based on names.  Explicit hyperlink targets have priority
over implicit targets.  But if an explicit target comes after an
implicit target with the same name, the ID of the first (implicit)
target remains based on the implicit name.  Since HTML fragment
identifiers are based on the IDs, the first target keeps the name.
For example:</p>
<pre class="literal-block">.. contents::

Section
=======

.. _contents:

Subsection
----------

text with a reference to contents_ and section_

.. _section:

This paragraph is explicitly targeted with the name &quot;section&quot;.</pre>
<p>When processed to HTML, the 2 internal hyperlinks (to &quot;contents&quot; &amp;
&quot;section&quot;) will work fine, but hyperlinks from outside the document
using <span class="docutils literal"><span class="pre">href=&quot;...#contents&quot;</span></span> and <span class="docutils literal"><span class="pre">href=&quot;...#section&quot;</span></span> won't work.
Such external links will connect to the implicit targets (table of
contents and &quot;Section&quot; title) instead of the explicit targets
(&quot;Subsection&quot; title and last paragraph).</p>
<p>Hyperlink targets with duplicate names should be assigned new IDs
unrelated to the target names (i.e., &quot;id&quot;-prefix serial IDs).</p>
</li>
<li><p>The &quot;contents&quot; ID of the local table of contents in
<span class="docutils literal">test/functional/expected/standalone_rst_pseudoxml.txt</span> is lost in
the HTML output at
<span class="docutils literal">test/functional/expected/standalone_rst_html4css1.html</span>.</p></li>
<li><p><span class="target" id="blank-first-columns">Blank first columns</span> in simple tables with explicit row separators
silently swallow their input.  They should at least produce system
error messages.  But, with explicit row separators, the meaning is
unambiguous and ought to be supported:</p>
<pre class="literal-block">==============  ==========
Table with row  separators
==============  ==========
                and blank
--------------  ----------
                entries
--------------  ----------
                in first
--------------  ----------
                columns.
==============  ==========</pre>
<p>Added a commented-out test case to
test/test_parsers/test_rst/test_SimpleTableParser.py.</p>
</li>
<li><p><span class="target" id="footnote-references-with-hyperlink-targets">Footnote references with hyperlink targets</span> cause a possibly
invalid node tree and make the HTML writer crash:</p>
<pre class="literal-block">$ rst2pseudoxml
[1]_

.. _1: URI
&lt;document source=&quot;&lt;stdin&gt;&quot;&gt;
    &lt;paragraph&gt;
        &lt;footnote_reference ids=&quot;id1&quot; refuri=&quot;URI&quot;&gt;
            1
    &lt;target ids=&quot;id2&quot; names=&quot;1&quot; refuri=&quot;URI&quot;&gt;</pre>
</li>
<li><p>Anonymous references have &quot;name&quot; attributes.  Should they?  Are they
used?  See <span class="docutils literal">test/test_parsers/test_rst/test_inline_markup.py</span>.</p></li>
<li><p>&lt;reference&gt; elements have a &quot;name&quot; attribute, not &quot;names&quot;.  The
attribute should be &quot;names&quot;; this is an inconsistency.</p></li>
</ul>
<!-- Local Variables:
mode: indented-text
indent-tabs-mode: nil
sentence-end-double-space: t
fill-column: 70
End: -->
</section>
</main>
<footer>
<p><a class="reference external" href="BUGS.txt">View document source</a>.
Generated on: 2023-09-13 17:06 UTC.
Generated by <a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> from <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> source.
</p>
</footer>
</body>
</html>
